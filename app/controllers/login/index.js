const { user_game } = require('../../../models');
const bcrypt = require('bcrypt');
const layout = 'layouts/main';
const format = (user) => {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
};

module.exports = {
  loginPage: (req, res) => {
    if (req.cookies.flashMsg) {
      const registered = req.cookies.registered;
      const flashMessage = req.cookies.flashMsg;
      res.cookie('flashMsg', '', { expires: new Date(0) });
      return res.render('web/login', {
        layout,
        title: 'LOG IN',
        message: flashMessage,
        messageClass: 'alert-danger',
        login: false,
        registered,
      });
    }
    res.render('web/login', {
      title: 'LOG IN',
      layout,
      message: '',
      login: false,
    });
  },
  loginForm: async (req, res) => {
    const { username, password } = req.body;
    const query = {
      where: {
        username: username,
      },
    };
    const user = await user_game.findOne(query);
    if (user) {
      const validPass = await bcrypt.compare(password, user.password);
      if (validPass) {
        const authToken = format(user);
        await res.cookie('authToken', authToken.accessToken, { maxAge: 86400000, httpOnly: true });
        res.redirect('/game');
      } else {
        res.cookie('flashMsg', `Invalid password for <b>${username}</b>!`);
        res.redirect('/login');
      }
    } else {
      res.cookie('flashMsg', `User with username <b>${username}</b> doesn't exist!`);
      res.redirect('/login');
    }
  },
};
