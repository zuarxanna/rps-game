const layout = 'layouts/main';
const jwt = require('jsonwebtoken');
const secret = 'suckerhead911';

const home = (req, res) => {
  const token = req.cookies.authToken;
  jwt.verify(token, secret, (err) => {
    if (err) {
      res.render('web/home', {
        layout,
        title: 'TRADITIONAL GAME',
        login: false,
      });
    } else {
      res.render('web/home', {
        layout,
        title: 'TRADITIONAL GAME',
        login: true,
      });
    }
  });
};

module.exports = home;
