const layout = 'layouts/main';
const { user_game, user_biodata } = require('../../../models');

module.exports = {
  signupPage: async (req, res) => {
    if (req.cookies.flashMsg) {
      const flashMessage = await req.cookies.flashMsg;
      const registUser = await req.cookies.registUser;
      res.cookie('flashMsg', '', { expires: new Date(0) });
      res.render('web/signup', {
        layout,
        title: 'SIGN UP',
        login: false,
        message: flashMessage,
        user: registUser,
      });
    } else {
      res.render('web/signup', {
        layout,
        title: 'SIGN UP',
        login: false,
        message: '',
      });
    }
  },
  signupForm: async (req, res) => {
    const { firstname, lastname, country, username, password } = await req.body;
    const newUser = await { username, password };
    const users = await user_game.findAll();
    if (users.find((i) => i.username === username)) {
      const registUser = await { firstname, lastname, country };
      res.cookie('flashMsg', `Username <b>${username}</b> already used!`);
      res.cookie('registUser', registUser, { maxAge: 20000 });
      res.redirect('/signup');
    } else {
      await user_game.signup(newUser);
      const user = await user_game.findOne({
        where: { username: username },
      });
      user_biodata.create({ firstname, lastname, country, userId: user.id });
      res.cookie('registered', true, { maxAge: 20000 });
      res.cookie('flashMsg', `Congratulation <b>${username}</b>, your account has been successfully created!`);
      res.redirect('/login');
    }
  },
};
