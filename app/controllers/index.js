const game = require('./game');
const home = require('./home');
const logout = require('./logout');
const { signupPage, signupForm } = require('./signup');
const { loginPage, loginForm } = require('./login');
const { dashboard, createUserPage, createUserForm, editUserPage, editUserForm, deleteUSer } = require('./dashboard');

module.exports = { game, home, logout, signupPage, signupForm, loginPage, loginForm, dashboard, createUserPage, createUserForm, editUserPage, editUserForm, deleteUSer };
