const logout = (req, res) => {
  function cookieLogout(cookie) {
    for (var prop in cookie) {
      if (!cookie.hasOwnProperty(prop)) {
        continue;
      }
      res.cookie(prop, '', { expires: new Date(0) });
    }
  }
  cookie = req.cookies;
  if (req.user) {
    req.logOut();
    cookieLogout(cookie);
    res.redirect('/');
  } else {
    cookieLogout(cookie);
    res.redirect('/');
  }
};

module.exports = logout;
