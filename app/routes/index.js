const router = require('express').Router();
const auth = require('../middleware/auth.js');
const home = require('./home');
const signup = require('./signup');
const login = require('./login');
const game = require('./game');
const dashboard = require('./dashboard');
const logout = require('./logout');
const room = require('./room');

router.use('/', home);
router.use('/login', login);
router.use('/dashboard', auth, dashboard);
router.use('/logout', logout);
router.use('/signup', signup);
router.use('/room', auth, room);
router.use('/game', auth, game);

module.exports = router;
