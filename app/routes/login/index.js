const router = require('express').Router();
const { loginPage, loginForm } = require('../../controllers/login');

router.get('/', loginPage);
router.post('/', loginForm);

module.exports = router;
