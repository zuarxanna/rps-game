const router = require('express').Router();
const { loginPageLocal, loginFormLocal } = require('../../controllers/auth-local');

router.get('/login', loginPageLocal);
router.post('/login', loginFormLocal);

module.exports = router;
