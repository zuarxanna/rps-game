const router = require('express').Router();
const { signupPage, signupForm } = require('../../controllers/signup');

router.get('/', signupPage);
router.post('/', signupForm);

module.exports = router;
