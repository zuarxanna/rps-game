const router = require('express').Router();
const { dashboard, createUserPage, createUserForm, editUserPage, editUserForm, deleteUSer } = require('../../controllers/dashboard');

router.get('/', dashboard);
router.get('/create-user', createUserPage);
router.post('/create-user', createUserForm);
router.get('/edit-user/:id', editUserPage);
router.post('/edit-user/:id', editUserForm);
router.get('/delete-user/:id', deleteUSer);

module.exports = router;
