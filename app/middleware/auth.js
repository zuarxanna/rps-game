const jwt = require('jsonwebtoken');
const secret = 'suckerhead911';

module.exports = (req, res, next) => {
  const rejectedMsg = () => {
    res.cookie('flashMsg', 'PLEASE LOGIN TO CONTINUE!');
    res.redirect('/login');
  };
  const token = req.cookies.authToken;
  if (token) {
    return jwt.verify(token, secret, (err, decodedToken) => {
      if (err) {
        return rejectedMsg();
      }
      req.user = decodedToken;
      return next();
    });
  }
  return rejectedMsg();
};
