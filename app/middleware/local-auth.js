module.exports = (req, res, next) => {
  if (req.isAuthenticated()) {
    res.cookie('authToken', '');
    return next();
  }
  res.redirect('auth-local/login');
};
