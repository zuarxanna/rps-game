const passport = require('passport');
const LocalStraegy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const { user_game } = require('../models');

const authenticate = async (username, password, done) => {
  const user = await user_game.findOne({ where: { username: username } });
  if (user == null) {
    return done(null, false, { message: `User with username <b>${username}</b> doesn't exist!` });
  }

  try {
    if (await bcrypt.compare(password, user.password)) {
      return done(null, user);
    } else {
      return done(null, false, { message: `Incorret password for <b>${username}</b>!` });
    }
  } catch (e) {
    return done(e);
  }
};
passport.use(new LocalStraegy({ usernameField: 'username' }, authenticate));
passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser(async (id, done) => {
  done(null, await user_game.findByPk(id));
});

module.exports = passport;
